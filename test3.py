import asyncio
import aiohttp
from bs4 import BeautifulSoup
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import (Column, Integer, String)
from loguru import logger
import schedule
import time


headers = {
    "Accept": "*/*",
    "User-Agent": 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/84.0.4147.135 Mobile Safari/537.36 '
}
host = 'https://m.habr.com'
URL = 'https://m.habr.com/ru/hub/health'

engine = create_engine('sqlite:///habr', echo=True)  # Подключаемся к БД

Base = declarative_base()  # определяет новый класс, который мы назвали Base, от которого будет унаследованы все наши ORM-классы

logger.add('habr_debug.log', format='{time} {level} {message}', level='DEBUG', rotation='100 KB', compression='zip')


class Article(Base):
    __tablename__ = 'article_data'
    # атрибуты класса
    id = Column(Integer, primary_key=True)
    date = Column(String)
    author_name = Column(String)
    nick_name = Column(String)
    name_link = Column(String)
    title = Column(String)
    project_urls = Column(String, unique=True, nullable=False)
    text = Column(String)

    # метод

    def __init__(self, date, author_name, nick_name, name_link, title, project_urls,
                 text):  # создание экземпляра класса Article
        self.date = date
        self.author_name = author_name
        self.nick_name = nick_name
        self.name_link = name_link
        self.title = title
        self.project_urls = project_urls
        self.text = text

    def __repr__(self):
        return "<article('%s','%s', '%s', '%s', '%s')>" % (
            self.date, self.author_name, self.nick_name, self.name_link, self.title)


class Hubs(Base):
    __tablename__ = 'hubs_data'

    id = Column(Integer, primary_key=True)
    url = Column(String)

    def __init__(self, url):
        self.url = url

    def __repr__(self):
        return "<hubs('%s')>" % (
            self.url)


Session = sessionmaker(bind=engine)  # Создание сессии, общение с базой
Session.configure(bind=engine)  # соедините с сессией
session = Session()

#####################
hubs1 = Hubs("https://m.habr.com/ru/hub/health/")
session.add(hubs1)


async def get_html(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url, headers=headers) as response:
            html = await response.text()
            # print(html)
            return html


async def get_content(html):
    soup = BeautifulSoup(html, 'lxml', )
    items = soup.find_all('article', class_='tm-articles-list__item')

    project_urls = []

    for item in items:
        project_url = host + item.find('a', class_='tm-article-snippet__title-link').get('href')
        project_urls.append(project_url)

    return project_urls
    # print(project_urls)


# async def main():
#
#     projects_data_list = []


async def parse_article(project_url):
    c = session.query(Article).filter_by(project_urls=project_url).count()
    if c > 0:
        return

    async with aiohttp.ClientSession() as s:
        async with s.get(project_url, headers=headers) as response:
            html = await response.text()

    soup = BeautifulSoup(html, 'lxml')

    author_name = soup.find('a', class_='tm-user-snippet__title').get_text(strip=True)
    if author_name:
        author_name = soup.find('a', class_='tm-user-snippet__title').get_text(strip=True)
    else:
        author_name = 'Имя не указано'

    try:
        date = soup.find('time', class_='tm-article-snippet__datetime-published').get('title')
    except AttributeError:
        logger.exception('date error!')

    a = Article(
        date=date,
        # date=soup.find('time', class_='tm-article-snippet__datetime-published').get('title'),
        author_name=author_name,
        nick_name=soup.find('a', class_='tm-user-snippet__nickname').get_text(strip=True),
        name_link=host + soup.find('a', class_='tm-user-snippet__title').get('href'),
        title=soup.find('h1', class_='tm-article-snippet__title').get_text(strip=True),
        project_urls=project_url,
        text=soup.find('div', id='post-content-body').get_text(strip=True),
    )

    session.add(a)
    session.commit()


@logger.catch
async def parse():
    task = []
    for i in session.query(Hubs).order_by(Hubs.id):
        url_list = i.url
        html = await get_html(url_list)
        content = await get_content(html)
        for cont in content:
            task.append(asyncio.create_task(parse_article(cont)))

        await asyncio.gather(*task)


def main():
    Base.metadata.create_all(engine)
    asyncio.run(parse())


if __name__ == '__main__':
    main()

schedule.every(1).minutes.do(main)

while True:
    schedule.run_pending()
    time.sleep(4)
